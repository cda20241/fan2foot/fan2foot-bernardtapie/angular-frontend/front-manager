import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {JoueurListeComponent} from "./pages/joueur/joueur-liste/joueur-liste.component";
import {RencontreDetailComponent} from "./pages/rencontre/rencontre-detail/rencontre-detail.component";
import { JoueurDetailComponent } from "./pages/joueur/joueur-detail/joueur-detail.component";
import {JoueurCreationComponent} from "./pages/joueur/joueur-creation/joueur-creation.component";
import {JoueurEditionComponent} from "./pages/joueur/joueur-edition/joueur-edition.component";
import { EquipeDetailComponent } from './pages/equipe/equipe-detail/equipe-detail.component';
import { RencontreListeComponent } from './pages/rencontre/rencontre-liste/rencontre-liste.component';
import { ListeEquipesComponent } from './pages/equipe/liste-equipes/liste-equipes.component';
import {RencontreResultatComponent} from "./pages/rencontre/rencontre-resultat/rencontre-resultat.component";
import { UtilisateurListeComponent } from './pages/utilisateur/utilisateur-liste/utilisateur-liste.component';
import { UtilisateurDetailComponent } from './pages/utilisateur/utilisateur-detail/utilisateur-detail.component';
import { UtilisateurEditComponent } from './pages/utilisateur/utilisateur-edit/utilisateur-edit.component';

const routes: Routes = [
  { path: "", redirectTo: "accueil", pathMatch: "full" },
  { path:'accueil', component: RencontreListeComponent},
  {path:'equipes', component: ListeEquipesComponent},
  { path:'joueurliste', component: JoueurListeComponent},
  { path:'rencontre', component: RencontreDetailComponent},
  { path:`joueurdetail`, component: JoueurDetailComponent},
  { path:'joueuredition', component: JoueurEditionComponent},
  { path:'joueurcreation', component: JoueurCreationComponent},
  { path:'equipedetail', component: EquipeDetailComponent},
  { path: 'joueur/:id', component: JoueurDetailComponent},
  { path: 'joueur/edit/:id', component: JoueurEditionComponent},
  { path: 'match-details/:id', component: RencontreDetailComponent},
  { path: 'match-resultat/:id', component: RencontreResultatComponent},
  { path: 'utilisateur/liste', component: UtilisateurListeComponent},
  { path : 'utilisateur/:id', component: UtilisateurDetailComponent},
  { path : 'utilisateur/edit/:id', component: UtilisateurEditComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {


}
