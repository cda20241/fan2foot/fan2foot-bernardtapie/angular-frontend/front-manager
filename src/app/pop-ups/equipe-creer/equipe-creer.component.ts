import { Component } from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {EquipeService} from "../../services/equipe.service";
import {Equipe} from "../../interfaces/equipe";

@Component({
  selector: 'app-equipe-creer',
  templateUrl: './equipe-creer.component.html',
  styleUrls: ['./equipe-creer.component.scss']
})
export class EquipeCreerComponent {
  constructor(private equipeService: EquipeService, public activeModal: NgbActiveModal) {
  }
  fermerModal(){
    this.activeModal.close()
  }
  creerEquipe() {
    let equipe: Equipe = {} as Equipe;
    equipe.nomLong = (<HTMLInputElement>document.getElementById('nomLong')).value;
    equipe.nomCourt = (<HTMLInputElement>document.getElementById('nomCourt')).value;
    equipe.championnat = (<HTMLInputElement>document.getElementById('championnat')).value;

    // S'assure que les inputs ne soient pas vides
    if (!equipe.nomLong || !equipe.nomCourt || !equipe.championnat) {
      let errorMessageElement = document.getElementById('errorMessage');
      if (errorMessageElement) {
        // Affiche une erreur si champs manquants.
        errorMessageElement.innerText = "Tous les champs doivent être remplis.";
        return;
      }
    }
    this.equipeService.creerEquipe(equipe).subscribe(
      response => {
        if (response.startsWith("L'équipe")) {
          // Ferme le popup si la création est validée
          this.activeModal.close();
          window.parent.location.reload();
        }
      },
      error => {
        let errorMessageElement = document.getElementById('errorMessage');
        if (errorMessageElement) {
          // Affiche l'erreur renvoyée par l'API
          errorMessageElement.innerText = error.error;
        }
      },
    );
  }
}
