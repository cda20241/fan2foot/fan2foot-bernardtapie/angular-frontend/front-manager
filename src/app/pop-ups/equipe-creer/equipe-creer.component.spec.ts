import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipeCreerComponent } from './equipe-creer.component';

describe('EquipeCreerComponent', () => {
  let component: EquipeCreerComponent;
  let fixture: ComponentFixture<EquipeCreerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EquipeCreerComponent]
    });
    fixture = TestBed.createComponent(EquipeCreerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
