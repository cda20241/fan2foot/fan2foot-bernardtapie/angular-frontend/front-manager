import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuuprimerEquipeComponent } from './supprimer-equipe.component';

describe('SuuprimerEquipeComponent', () => {
  let component: SuuprimerEquipeComponent;
  let fixture: ComponentFixture<SuuprimerEquipeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SuuprimerEquipeComponent]
    });
    fixture = TestBed.createComponent(SuuprimerEquipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
