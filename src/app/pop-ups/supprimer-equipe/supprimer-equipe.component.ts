import { Component, inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EquipeService } from 'src/app/services/equipe.service';

@Component({
  selector: 'app-suuprimer-equipe',
  templateUrl: './supprimer-equipe.component.html',
  styleUrls: ['./supprimer-equipe.component.scss']
})
export class SupprimerEquipeComponent {
  modal = inject(NgbActiveModal);
  idEquipe!:number;
  constructor(private equipeService: EquipeService, public activeModal: NgbActiveModal) {}

  //méthode pour confirmer la suppression de l'équipe
  supprimerEquipe(){
    this.equipeService.supprimerEquipe(this.idEquipe).subscribe(
      response => {
        console.log(response);
        this.modal.close('deleted');
      },
      // le message de Response arrive souvent dans error : problème de typage !
      error => {
        this.modal.close('deleted');
      }
    );
  }
}
