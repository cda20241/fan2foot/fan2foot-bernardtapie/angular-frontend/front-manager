import { Component } from '@angular/core';
import { Rencontre } from "../../interfaces/rencontre";
import { RencontreService } from '../../services/rencontre.service';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {Equipe} from "../../interfaces/equipe";
import {EquipeService} from "../../services/equipe.service";
@Component({
  selector: 'app-match-creer',
  templateUrl: './match-creer.component.html',
  styleUrls: ['./match-creer.component.scss']
})
export class MatchCreerComponent {
  selectedEquipe1!: Equipe;
  selectedEquipe2!: Equipe;

  equipeList?: Equipe[];
  constructor(private rencontreService: RencontreService, public activeModal: NgbActiveModal, private equipeService: EquipeService) {
  }
  ngOnInit(): void {
    this.equipeService.getEquipeList().subscribe(equipeList =>
      this.equipeList = equipeList);
    console.log(this.equipeList)
  }
  fermerModal(){
    this.activeModal.close()
  }
  creerRencontre() {
    // Récupération des inputs du HTML
    let team1 = this.selectedEquipe1.id
    let team2 = this.selectedEquipe2.id
    let date = (<HTMLInputElement>document.getElementById('date')).value;
    let time = (<HTMLInputElement>document.getElementById('time')).value + ":00";

    // S'assure que la date ou l'heure ne sont pas vide
    if (!date || !time) {
      let errorMessageElement = document.getElementById('errorMessage');
      if (errorMessageElement) {
        // Affiche une erreur si champs manquants.
        errorMessageElement.innerText = "Tous les champs doivent être remplis.";
        return;
      }
    }
    // Instanciation de rencontre avec les valeurs récupérées du html
    let rencontre: Rencontre = {
      idEqDomicile: team1,
      idEqExterieur: team2,
      date: date,
      heure: time
    } as Rencontre;

    //Créer la rencontre et vérifie qu'elle a bien reçu un retour positif de l'API
    this.rencontreService.creerRencontre(rencontre).subscribe(
      response => {
        if (response.startsWith("La rencontre n°")) {
          // Ferme le popup si la création est validée
          this.activeModal.close();
          window.parent.location.reload();
        }
      },
      //Affiche un message d'erreur si l'API en retourne un
      error => {
        let errorMessageElement = document.getElementById('errorMessage');
        if (errorMessageElement) {
          // Affiche l'erreur renvoyée par l'API
          errorMessageElement.innerText = error.error;
        }
      },
    );
  }
}

