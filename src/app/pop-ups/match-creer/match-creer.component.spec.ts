import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchCreerComponent } from './match-creer.component';

describe('MatchCreerComponent', () => {
  let component: MatchCreerComponent;
  let fixture: ComponentFixture<MatchCreerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MatchCreerComponent]
    });
    fixture = TestBed.createComponent(MatchCreerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
