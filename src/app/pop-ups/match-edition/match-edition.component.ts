import { Component, Input } from '@angular/core';
import { Rencontre } from "../../interfaces/rencontre";
import { RencontreService } from '../../services/rencontre.service';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {Match} from "../../interfaces/match";

@Component({
  selector: 'app-match-edition',
  templateUrl: './match-edition.component.html',
  styleUrls: ['./match-edition.component.scss']
})
export class MatchEditionComponent {
  @Input() match!: Match;

  constructor(private rencontreService: RencontreService, public activeModal: NgbActiveModal) {
  }

  fermerModal(){
    this.activeModal.close()
  }
  editerRencontre() {
    let date = (<HTMLInputElement>document.getElementById('date')).value;
    let time = (<HTMLInputElement>document.getElementById('time')).value + ":00";

    // S'assure que la date ou l'heure ne sont pas vide
    if (!date || !time) {
      let errorMessageElement = document.getElementById('errorMessage');
      if (errorMessageElement) {
        // Affiche une erreur si champs manquants.
        errorMessageElement.innerText = "La Date et Heure doivent être indiqués.";
        return;
      }
    }

    let rencontre:Rencontre = {
      id: this.match.id,
      date: date,
        heure: time
    };

    this.rencontreService.editerRencontre(rencontre).subscribe(
      response => {
        if (response.startsWith("La rencontre n°")) {
          // Ferme le popup si l'édition est validée
          this.activeModal.close('edited');
        }
      },
      error => {
        let errorMessageElement = document.getElementById('errorMessage');
        if (errorMessageElement) {
          // Affiche l'erreur renvoyée par l'API
          errorMessageElement.innerText = error.error;
        }
      },
    );
  }


}
