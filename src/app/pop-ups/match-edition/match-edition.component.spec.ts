import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchEditionComponent } from './match-edition.component';

describe('MatchEditionComponent', () => {
  let component: MatchEditionComponent;
  let fixture: ComponentFixture<MatchEditionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MatchEditionComponent]
    });
    fixture = TestBed.createComponent(MatchEditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
