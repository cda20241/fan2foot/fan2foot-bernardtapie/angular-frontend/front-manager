import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupprimerMatchComponent } from './supprimer-match.component';

describe('SupprimerMatchComponent', () => {
  let component: SupprimerMatchComponent;
  let fixture: ComponentFixture<SupprimerMatchComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SupprimerMatchComponent]
    });
    fixture = TestBed.createComponent(SupprimerMatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
