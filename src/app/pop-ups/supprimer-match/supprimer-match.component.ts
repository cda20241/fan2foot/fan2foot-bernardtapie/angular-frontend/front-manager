import {Component, inject} from '@angular/core';
import {RencontreService} from "../../services/rencontre.service";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import { Match } from "../../interfaces/match";

@Component({
  selector: 'app-supprimer-match',
  templateUrl: './supprimer-match.component.html',
  styleUrls: ['./supprimer-match.component.scss']
})
export class SupprimerMatchComponent {
  matchId! : number;
  modal = inject(NgbActiveModal);
  constructor(private rencontreService: RencontreService, public activeModal: NgbActiveModal) {}

  //méthode pour confirmer la suppression
  supprimerRencontre(){
    this.rencontreService.supprimerRencontre(this.matchId).subscribe(
      response => {
        console.log(response);
        this.modal.close('deleted');
      },
      // le message de Response arrive souvent dans error : problème de typage !      
      error => {
        this.modal.close('deleted');
      }          
    );
  }
}
