import { Component, Input } from '@angular/core';
import { Equipe } from 'src/app/interfaces/equipe';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import { EquipeService } from 'src/app/services/equipe.service';
import { Joueur } from 'src/app/interfaces/joueur';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';

/**
 * Fonction pour restreindre l'utilisateur à n'utiliser que les les lettres de A à Z
 * @returns 
 */
function noSpecialChars(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const forbidden = /[^a-zA-Z \-]/.test(control.value);
    return forbidden ? {'noSpecialChars': {value: control.value}} : null;
  };
}

@Component({
  selector: 'app-equipe-edition',
  templateUrl: './equipe-edition.component.html',
  styleUrls: ['./equipe-edition.component.scss']
})
export class EquipeEditionComponent {
@Input() equipe! : Equipe;
joueur! : Joueur;
formEquipe: FormGroup = new FormGroup({
  nomCourt: new FormControl(this.equipe, [Validators.required, noSpecialChars()]),
  nomLong: new FormControl(this.equipe, [Validators.required, noSpecialChars()]),
  championnat: new FormControl(this.equipe)
})

constructor(
  private equipeService: EquipeService,  
  public activeModal: NgbActiveModal) {
}
fermerModal(){
  this.activeModal.close()
}
valider() {
  if (this.formEquipe.valid){ 
      this.fermerModal()
      let equipe: Equipe = {} as Equipe;   
      equipe.nomLong=this.formEquipe.get("nomLong")!.value as string;
      equipe.nomCourt = (this.formEquipe.get("nomCourt")!.value as string).toUpperCase();
      equipe.championnat=this.formEquipe.get("championnat")!.value as string;
      equipe.archive=false
      console.log(equipe.id, equipe)
      this.equipeService.updateEquipe(this.equipe.id, equipe).subscribe(res => console.log(res))
     
}
}
}
