import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipeEditionComponent } from './equipe-edition.component';

describe('EquipeEditionComponent', () => {
  let component: EquipeEditionComponent;
  let fixture: ComponentFixture<EquipeEditionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EquipeEditionComponent]
    });
    fixture = TestBed.createComponent(EquipeEditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
