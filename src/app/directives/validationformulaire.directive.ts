import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[appValidationformulaire]'
})
export class ValidationformulaireDirective {


  constructor(private el: ElementRef, private control: NgControl) {}

  @HostListener('ngModelChange')
  onModelChange() {
    this.highlight(this.control.valid!);
  }

  private highlight(isValid: boolean) {
    if (isValid) {
      this.el.nativeElement.classList.add('valid');
    } else {
      this.el.nativeElement.classList.remove('valid');
    }
  }
}