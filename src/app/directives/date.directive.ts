import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appDate]'
})
export class DateDirective {

  constructor(private el: ElementRef) {
    this.el.nativeElement.type ='text';
   }
   @HostListener('focus')
   onFocus() {
     this.el.nativeElement.type = 'date';
   }
 
   @HostListener('blur')
   onBlur() {
     if (this.el.nativeElement.value === '') {
       this.el.nativeElement.type = 'text';
     }
   }
}
