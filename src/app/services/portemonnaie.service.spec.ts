import { TestBed } from '@angular/core/testing';

import { PortemonnaieService } from './portemonnaie.service';

describe('PortemonnaieService', () => {
  let service: PortemonnaieService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PortemonnaieService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
