import { Injectable } from '@angular/core';
import { environement } from 'src/environment/environment';
import {Equipe, EquipeJoueurs} from '../interfaces/equipe';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EquipeService {

  apiUrl=environement.apiUrl+'/equipe'
  afichageListeAccueil=false;
  constructor(
    private http: HttpClient,
  ) { }

  getEquipeList(): Observable<Equipe[]> {
    return this.http.get<Equipe[]>(this.apiUrl + '/equipes_sans_joueurs' );
  }

  getEquipe(id:number): Observable<EquipeJoueurs>{
    return this.http.get<EquipeJoueurs>(this.apiUrl+"/" + id)
  }

  creerEquipe(equipe: Equipe): Observable<any> {
    return this.http.post(this.apiUrl+'/creer', equipe, {responseType: 'text'});
  }

  updateEquipe(id: number, equipe: Equipe): Observable<string>{
    return this.http.put<string>(this.apiUrl + '/modifier/' + id, equipe)
  }

  supprimerEquipe(id:number) : Observable<string>{
    return this.http.delete<string>(this.apiUrl + '/supprimer/' + id);
  }

}
