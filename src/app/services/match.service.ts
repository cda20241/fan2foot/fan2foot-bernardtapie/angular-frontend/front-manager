import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {forkJoin, map, Observable, switchMap} from 'rxjs';
import { environement } from 'src/environment/environment';
import { HttpHeaders } from '@angular/common/http';
import { Match } from '../interfaces/match';
import { MatchDetails } from '../interfaces/matchDetails';
import {EquipeService} from "./equipe.service";

@Injectable({
  providedIn: 'root'
})

export class MatchService {

    private apiUrl = environement.apiUrl+'/rencontre/'
    httpOptions = {headers: new HttpHeaders({ 'Content-Type': 'application/json' })};

    constructor(private http: HttpClient,private equipeService: EquipeService) {}

    getListeMatchs():Observable<Match[]> {
      return this.http.get<Match[]>(this.apiUrl + 'liste')
    }
  getMatchDetails(matchId: number): Observable<MatchDetails> {
    return this.http.get<MatchDetails>(this.apiUrl + matchId).pipe(
      switchMap((matchDetails: MatchDetails) => {
        return forkJoin({
          eqDomicile: this.equipeService.getEquipe(matchDetails.eqDomicile.id),
          eqExterieur: this.equipeService.getEquipe(matchDetails.eqExterieur.id)
        }).pipe(
          map(({eqDomicile, eqExterieur}) => {
            matchDetails.eqDomicile.joueurs = eqDomicile.joueurs;
            matchDetails.eqExterieur.joueurs = eqExterieur.joueurs;
            return matchDetails;
          })
        );
      })
    );
  }
}
