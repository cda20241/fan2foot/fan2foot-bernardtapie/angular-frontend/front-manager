import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environement } from 'src/environment/environment';
import { Portemonnaie } from '../interfaces/portemonnaie';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PortemonnaieService {
  apiUrl=environement.apiPari;
  
  constructor(
    private http: HttpClient,
  ) { }


  getPorteMonnaie(id: number): Observable<Portemonnaie>{
    return this.http.get<Portemonnaie>(this.apiUrl + '/portemonnaie/'+id );
  }

}


