import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {Rencontre} from "../interfaces/rencontre";
import {environement} from "../../environment/environment";
import {JoueursDetails, MatchDetails} from "../interfaces/matchDetails";

@Injectable({
  providedIn: 'root'
})
export class RencontreService {
  private rencontresUrl = environement.apiUrl+"/rencontre";  // URL to web api
  private scoreDomicile = new BehaviorSubject(0);
  private scoreExterieur = new BehaviorSubject(0);
  private butsDomicile = new BehaviorSubject<JoueursDetails[]>([]);
  private butsExterieur = new BehaviorSubject<JoueursDetails[]>([]);
  private passesDomicile = new BehaviorSubject<JoueursDetails[]>([]);
  private passesExterieur = new BehaviorSubject<JoueursDetails[]>([]);
  actuelScoreDomicile = this.scoreDomicile.asObservable();
  actuelScoreExterieur = this.scoreExterieur.asObservable();
  actuelButsDomicile = this.butsDomicile.asObservable();
  actuelButsExterieur = this.butsExterieur.asObservable();
  actuelPassesDomicile = this.passesDomicile.asObservable();
  actuelPassesExterieur = this.passesExterieur.asObservable();

  constructor(private http: HttpClient) { }

  creerRencontre(rencontre: Rencontre): Observable<any> {
    return this.http.post(this.rencontresUrl+'/creer', rencontre, {responseType: 'text'});
  }

  editerRencontre(rencontre: Rencontre): Observable<any> {
    return this.http.put(this.rencontresUrl+'/modifier/'+rencontre.id, rencontre, {responseType: 'text'});
  }

  sauvegarderResultat(matchDetails:any): Observable<any>{
    return this.http.post(this.rencontresUrl+'/resultat',matchDetails,{responseType:'text'});
  }

  supprimerRencontre(id:number): Observable<string>{
    return this.http.delete<string>(this.rencontresUrl + '/supprimer/'+id);
  }
  changeScoreDomicile(score: number) {
    this.scoreDomicile.next(score);
  }

  changeScoreExterieur(score: number) {
    this.scoreExterieur.next(score);
  }
  changeButsDomicile(buts: any[]) {
    this.butsDomicile.next(buts);
  }

  changeButsExterieur(buts: any[]) {
    this.butsExterieur.next(buts);
  }

  changePassesDomicile(passes: any[]) {
    this.passesDomicile.next(passes);
  }

  changePassesExterieur(passes: any[]) {
    this.passesExterieur.next(passes);
  }
}
