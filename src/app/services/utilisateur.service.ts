import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environement } from 'src/environment/environment';
import { Utilisateur, UtilisateurIdRole } from '../interfaces/utilisateur';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {
  apiUrl=environement.apiUtilisateur+'/utilisateur'
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  
  constructor(
    private http: HttpClient,
  ) { }

  /**
   * Fonction qui va chercher dans l'API Utilisateur la liste des Utilisateurs avec leur role
   * @returns 
   */
  getUtilisateursAvecRole(): Observable<Utilisateur[]> {
    return this.http.get<Utilisateur[]>(this.apiUrl + '/liste' );
  }

  getUtilisateur(id: number): Observable<Utilisateur>{
    return this.http.get<Utilisateur>(this.apiUrl + '/'+id );
  }

  deleteUtilisateur(id: number): Observable<string>{
    return this.http.delete<string>(this.apiUrl + '/supprimer/'+id );
  }


  updateUtilisateur(id : number, utilisateur: UtilisateurIdRole): Observable<string>{
    console.log("UTILISATEUR PRENOM:" + utilisateur.prenom + "UTILISATEUR MDP : " + utilisateur.mdp + "UTILISATEUR DOB : "+ utilisateur.date_naissance)
    return this.http.put<string>(this.apiUrl + '/modifier/'+id, utilisateur);
  }
}
