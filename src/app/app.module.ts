import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { JoueurListeComponent } from './pages/joueur/joueur-liste/joueur-liste.component';
import { RencontreDetailComponent } from './pages/rencontre/rencontre-detail/rencontre-detail.component';
import { JoueurDetailComponent } from './pages/joueur/joueur-detail/joueur-detail.component';
import { RencontreListeComponent } from './pages/rencontre/rencontre-liste/rencontre-liste.component';
import { JoueurEditionComponent } from './pages/joueur/joueur-edition/joueur-edition.component';
import { JoueurCreationComponent } from './pages/joueur/joueur-creation/joueur-creation.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { MatchsListeComponent } from './components/matchs-liste/matchs-liste.component';
import { MatchInfoComponent } from './components/match-info/match-info.component';
import { ProfilePreviewComponent } from './components/profile-preview/profile-preview.component';
import { ClassementComponent } from './components/classement/classement.component';
import { EquipeDomicileComponent } from './components/equipe-domicile/equipe-domicile.component';
import { EquipeVisiteuseComponent } from './components/equipe-visiteuse/equipe-visiteuse.component';
import { JoueursListeComponent } from './components/joueurs-liste/joueurs-liste.component';
import { JoueurComponent } from './components/joueur/joueur.component';
import { EquipeDetailComponent } from './pages/equipe/equipe-detail/equipe-detail.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import {InformationsMatchComponent} from "./components/informations-match/informations-match.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatchCreerComponent } from './pop-ups/match-creer/match-creer.component';
import { EquipesListeComponent } from './components/equipes-liste/equipes-liste.component';
import { SupprimerMatchComponent } from './pop-ups/supprimer-match/supprimer-match.component';
import {MatchEditionComponent} from "./pop-ups/match-edition/match-edition.component";
import { EquipeCreerComponent } from './pop-ups/equipe-creer/equipe-creer.component';
import { ListeEquipesComponent } from './pages/equipe/liste-equipes/liste-equipes.component';
import { EquipeEditionComponent } from './pop-ups/equipe-edition/equipe-edition.component';
import {SupprimerEquipeComponent} from "./pop-ups/supprimer-equipe/supprimer-equipe.component";
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { RencontreResultatComponent } from './pages/rencontre/rencontre-resultat/rencontre-resultat.component';
import { EquipeDomicileEditionComponent } from './components/equipe-domicile-edition/equipe-domicile-edition.component';
import { EquipeVisiteuseEditionComponent } from './components/equipe-visiteuse-edition/equipe-visiteuse-edition.component';
import { UtilisateurListeComponent } from './pages/utilisateur/utilisateur-liste/utilisateur-liste.component';
import { UtilisateurDetailComponent } from './pages/utilisateur/utilisateur-detail/utilisateur-detail.component';
import { UtilisateurEditComponent } from './pages/utilisateur/utilisateur-edit/utilisateur-edit.component';
import { DateDirective } from './directives/date.directive';
import { ValidationformulaireDirective } from './directives/validationformulaire.directive';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    JoueurListeComponent,
    RencontreDetailComponent,
    JoueurDetailComponent,
    RencontreListeComponent,
    JoueurEditionComponent,
    JoueurCreationComponent,
    PaginationComponent,
    MatchsListeComponent,
    MatchInfoComponent,
    ProfilePreviewComponent,
    ClassementComponent,
    EquipeDomicileComponent,
    EquipeVisiteuseComponent,
    JoueursListeComponent,
    JoueurComponent,
    EquipeDetailComponent,
    InformationsMatchComponent,
    MatchCreerComponent,
    EquipesListeComponent,
    SupprimerMatchComponent,
    MatchEditionComponent,
    EquipeCreerComponent,
    ListeEquipesComponent,
    MatchEditionComponent,
    EquipeEditionComponent,
    SupprimerEquipeComponent,
    RencontreResultatComponent,
    EquipeDomicileEditionComponent,
    EquipeVisiteuseEditionComponent,
    UtilisateurListeComponent,
    UtilisateurDetailComponent,
    UtilisateurEditComponent,
    DateDirective,
    ValidationformulaireDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr' } // Définit la locale par défaut en français
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    registerLocaleData(localeFr, 'fr'); // Enregistre la configuration de la locale française
  }
 }
