import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipeDomicileEditionComponent } from './equipe-domicile-edition.component';

describe('EquipeDomicileEditionComponent', () => {
  let component: EquipeDomicileEditionComponent;
  let fixture: ComponentFixture<EquipeDomicileEditionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EquipeDomicileEditionComponent]
    });
    fixture = TestBed.createComponent(EquipeDomicileEditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
