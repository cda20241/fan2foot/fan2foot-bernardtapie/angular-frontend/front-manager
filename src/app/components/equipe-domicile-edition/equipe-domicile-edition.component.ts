import {Component, Input, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {EquipeDetails, JoueursDetails, MatchDetails} from "../../interfaces/matchDetails";
import {RencontreService} from "../../services/rencontre.service";

@Component({
  selector: 'app-equipe-domicile-edition',
  templateUrl: './equipe-domicile-edition.component.html',
  styleUrls: ['./equipe-domicile-edition.component.scss']
})
export class EquipeDomicileEditionComponent implements OnInit {

  @Input() matchDetails$: Observable<MatchDetails> | any;
  equipeDomicile: EquipeDetails | undefined;
  buteurs: JoueursDetails[] = [];
  passeurs: JoueursDetails[] = [];
  buteursId: number[] = [];
  passeursId: number[] = [];
  joueurSelectionneButeur: JoueursDetails | undefined;
  joueurSelectionnePasseur: JoueursDetails | undefined;

  constructor(private rencontreService : RencontreService) {
  }

  ngOnInit(): void {
    if (this.matchDetails$) {
      this.matchDetails$.subscribe({
        next: (details: MatchDetails) => {
          if (details && details.eqDomicile) {
            this.equipeDomicile = details.eqDomicile;
            console.log('Détails de l\'équipe domicile :', this.equipeDomicile);
          } else {
            console.error('Les détails de l\'équipe domicile n\'ont pas été fournis.');
          }
        },
        error: (error: any) => {
          console.error('Erreur lors de la récupération des détails du match :', error);
        }
      });
    } else {
      console.error('Aucun détail de match fourni.');
    }
  }

  ajouterButeur() {
    if (this.joueurSelectionneButeur) {
      this.buteurs.push(this.joueurSelectionneButeur);
      this.buteursId.push(this.joueurSelectionneButeur.id)
      this.rencontreService.changeButsDomicile(this.buteursId);
      this.rencontreService.changeScoreDomicile(this.buteurs.length);
    }
  }

  ajouterPasseur() {
    if (this.joueurSelectionnePasseur) {
      this.passeurs.push(this.joueurSelectionnePasseur);
      this.passeursId.push(this.joueurSelectionnePasseur.id)
      this.rencontreService.changePassesDomicile(this.passeursId);
    }
  }

  supprimerButeur(index: number) {
    this.buteurs.splice(index, 1);
    this.buteursId.splice(index, 1);
    this.rencontreService.changeButsDomicile(this.buteursId);
    this.rencontreService.changeScoreDomicile(this.buteurs.length);
  }

  supprimerPasseur(index: number) {
    this.passeurs.splice(index, 1);
    this.passeursId.splice(index, 1)
    this.rencontreService.changePassesDomicile(this.passeursId);
  }

  /* ----- AFFICHAGE DU LOGO DES EQUIPE ----- */
  getTeamLogo(nomLong: string): string {
    switch (nomLong.toLowerCase()) {
      case 'glasgow ranger':
        return '../assets/images/equipeLogo/glasgowRangers.png';
      case 'celtic glasgow':
        return '../assets/images/equipeLogo/celtic.png';
      case 'heart of midlothian fc':
        return '../assets/images/equipeLogo/heart.png';
      case 'kilmarnock fc':
        return '../assets/images/equipeLogo/kilmarnock.png';
      case 'st mirren fc':
        return '../assets/images/equipeLogo/saintMirren.png';
      case 'hibernian fc':
        return '../assets/images/equipeLogo/hibernian.png';
      case 'dundee fc':
        return '../assets/images/equipeLogo/dundee.png';
      case 'motherwell fc':
        return '../assets/images/equipeLogo/motherwell.png';
      case 'aberdeen fc':
        return '../assets/images/equipeLogo/aberdeen.png';
      case 'st johnstone fc':
        return '../assets/images/equipeLogo/stJohnstone.png';
      case 'ross county':
        return '../assets/images/equipeLogo/rossCounty.png';
      case 'livingston fc':
        return '../assets/images/equipeLogo/livingstone.png';
      default:
        return '';
    }
  }

}

