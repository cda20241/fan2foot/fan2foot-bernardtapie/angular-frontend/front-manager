import { Component, Input, OnInit } from '@angular/core';
import { EquipeDetails, MatchDetails } from 'src/app/interfaces/matchDetails';
import { Observable } from 'rxjs';
import {Match} from "../../interfaces/match";

@Component({
  selector: 'app-equipe-domicile',
  templateUrl: './equipe-domicile.component.html',
  styleUrls: ['./equipe-domicile.component.scss']
})
export class EquipeDomicileComponent implements OnInit {

  @Input() matchDetails$: Observable<MatchDetails> | any;
  equipeDomicile: EquipeDetails | undefined;
  match?:MatchDetails
  constructor() {}

  ngOnInit(): void {
    if (this.matchDetails$) {
      this.matchDetails$.subscribe({
        next: (details: MatchDetails) => {
          if (details && details.eqDomicile) {
            this.match = details;
            this.equipeDomicile = details.eqDomicile;
            console.log('Détails de l\'équipe domicile :', this.equipeDomicile);
          } else {
            console.error('Les détails de l\'équipe domicile n\'ont pas été fournis.');
          }
        },
        error: (error: any) => {
          console.error('Erreur lors de la récupération des détails du match :', error);
        }
      });
    } else {
      console.error('Aucun détail de match fourni.');
    }
  }

  /* ----- AFFICHAGE DU LOGO DES EQUIPE ----- */
  getTeamLogo(nomLong: string): string {
    switch (nomLong.toLowerCase()) {
      case 'glasgow ranger':
        return '../assets/images/equipeLogo/glasgowRangers.png';
      case 'celtic glasgow':
        return '../assets/images/equipeLogo/celtic.png';
      case 'heart of midlothian fc':
        return '../assets/images/equipeLogo/heart.png';
      case 'kilmarnock fc':
        return '../assets/images/equipeLogo/kilmarnock.png';
      case 'st mirren fc':
        return '../assets/images/equipeLogo/saintMirren.png';
      case 'hibernian fc':
        return '../assets/images/equipeLogo/hibernian.png';
      case 'dundee fc':
        return '../assets/images/equipeLogo/dundee.png';
      case 'motherwell fc':
        return '../assets/images/equipeLogo/motherwell.png';
      case 'aberdeen fc':
        return '../assets/images/equipeLogo/aberdeen.png';
      case 'st johnstone fc':
        return '../assets/images/equipeLogo/stJohnstone.png';
      case 'ross county':
        return '../assets/images/equipeLogo/rossCounty.png';
      case 'livingston fc':
        return '../assets/images/equipeLogo/livingstone.png';
      default:
        return '';
    }
  }

}
