import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {JoueursDetails, MatchDetails} from "../../interfaces/matchDetails";
import {RencontreService} from "../../services/rencontre.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-match-info',
  templateUrl: './match-info.component.html',
  styleUrls: ['./match-info.component.scss'],
  providers: [DatePipe]
})
export class MatchInfoComponent implements OnInit {
  @Input() matchDetails$: Observable<MatchDetails> | any;
  match!:MatchDetails;
  scoreDomicile!:number;
  scoreExterieur!:number;
  @Input() date!: string;
  @Input() heure!: string;
  butsDomicile!:JoueursDetails[];
  butsExterieur!:JoueursDetails[];
  passesDomicile!: JoueursDetails[];
  passesExterieur!: JoueursDetails[];
  jour?:string;
  jourMois?:string;
  heureMinutes?:string;
  statutMatch?:string;
  resultsaved?:boolean;

  constructor(private datePipe:DatePipe, private rencontreService:RencontreService, private router:Router){}
  message: string = '';

  ngOnInit(): void {
    if (this.matchDetails$) {
      this.rencontreService.actuelScoreDomicile.subscribe(score => this.scoreDomicile = score);
      this.rencontreService.actuelScoreExterieur.subscribe(score => this.scoreExterieur = score);
      this.rencontreService.actuelButsDomicile.subscribe(buts => this.butsDomicile = buts);
      this.rencontreService.actuelButsExterieur.subscribe(buts => this.butsExterieur = buts);
      this.rencontreService.actuelPassesDomicile.subscribe(passes => this.passesDomicile = passes);
      this.rencontreService.actuelPassesExterieur.subscribe(passes => this.passesExterieur = passes);
      this.matchDetails$.subscribe({
        next: (details: MatchDetails) => {
          this.match = details;
          this.scoreExterieur = details.scoreExterieur;
          this.scoreDomicile = details.scoreDomicile;
          this.jour=this.extraireNomJour(this.match.date)!;
          this.jourMois=this.extraireJourEtMois(this.match.date)!;
          this.heureMinutes=this.extraireHeureEtMinute(this.match.heure);
          this.statutMatch= this.calculerStatutMatch(this.match.date, this.match.heure);
        },
        error: (error: any) => {
          console.error('Erreur lors de la récupération des détails du match :', error);
        }
      });
    } else {
      console.error('Aucun détail de match fourni.');
    }

  }
  MatchResultatRoute() {
    return this.router.url === '/match-resultat/'+this.match.id;
  }
  // méthode pour extraire le nom du jour
  extraireNomJour(dateString: string) {
    const date = new Date(dateString);
    return this.datePipe.transform(date, 'EEEE', 'fr');
  }

  // méthode pour extraire le jour et le mois
  extraireJourEtMois(dateString: string) {
    const date = new Date(dateString);
    return this.datePipe.transform(date, 'dd MMMM', 'fr');
  }

  // méthode pour extraire l'heure sous format 00H00
  extraireHeureEtMinute(timeString: string): string {
    const [heure, minute] = timeString.split(':');
    const heureFormat = heure + 'H';
    const minuteFormat = minute;
    return `${heureFormat}${minuteFormat}`;
  }

  // méthode pour calculer le statut du match en fonction de l'heure et la date
  calculerStatutMatch(dateString: string, timeString: string): string {
    const dateMatch = new Date(dateString);
    const [heure, minute] = timeString.split(':');
    dateMatch.setHours(parseInt(heure, 10), parseInt(minute, 10), 0, 0);
    console.log(dateMatch)
    const maintenant = new Date();
    console.log(maintenant)
    const differenceMinutes = Math.round((dateMatch.getTime() - maintenant.getTime()) / (1000 * 60));
    console.log(differenceMinutes)
    if (differenceMinutes > 90) {
      return 'À venir';
    } else if (differenceMinutes <= 0 && differenceMinutes >= -90) {
      return 'En cours';
    } else {
      return 'Terminé';
    }
  }
  sauvegarderResultat(): void {
    let matchDetails = {
      idRencontre: this.match.id,
      butsExterieur: this.butsExterieur,
      butsDomicile: this.butsDomicile,
      passesDomicile: this.passesDomicile,
      passesExterieur: this.passesExterieur,
      scoreDomicile: this.scoreDomicile,
      scoreExterieur: this.scoreExterieur
    };

    this.rencontreService.sauvegarderResultat(matchDetails).subscribe({
      next: (response: any) => {
        console.log('Détails du match sauvegardés avec succès :', response);
        this.message = 'Détails du match sauvegardés avec succès';
        this.resultsaved=true
      },
      error: (error: any) => {
        console.error('Erreur lors de la sauvegarde des détails du match :', error);
        this.message = 'Erreur lors de la sauvegarde des détails du match';
      }
    });
  }
}
