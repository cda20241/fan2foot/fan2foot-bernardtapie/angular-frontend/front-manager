import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipeVisiteuseEditionComponent } from './equipe-visiteuse-edition.component';

describe('EquipeVisiteuseEditionComponent', () => {
  let component: EquipeVisiteuseEditionComponent;
  let fixture: ComponentFixture<EquipeVisiteuseEditionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EquipeVisiteuseEditionComponent]
    });
    fixture = TestBed.createComponent(EquipeVisiteuseEditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
