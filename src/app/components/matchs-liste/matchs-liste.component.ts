import { Component, OnInit } from "@angular/core";
import { Match } from "src/app/interfaces/match";
import { MatchService } from "src/app/services/match.service";
import {MatchCreerComponent} from "../../pop-ups/match-creer/match-creer.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {SupprimerMatchComponent} from "../../pop-ups/supprimer-match/supprimer-match.component";
import {MatchEditionComponent} from "../../pop-ups/match-edition/match-edition.component";
import {DatePipe} from "@angular/common";
import {Router} from "@angular/router";


@Component({
  selector: 'app-matchs-liste',
  templateUrl: './matchs-liste.component.html',
  styleUrls: ['./matchs-liste.component.scss'],
  providers: [DatePipe]

})
export class MatchsListeComponent implements OnInit {
  matchs: Match[] = [];
  jour?:string;
  heureMinutes?:string;

    // VARIABLE DE PAGINATION
    paginationCurrentPage: number = 1; // Page actuelle
    paginationItemsPerPage: number = 4; // Nombre d'éléments par page
    paginatedItems: Match[] = []; // Joueurs paginés pour la vue actuelle
    maxPagesToShow: number = 15; // Nombre maximum de pages à afficher

  constructor(private matchService: MatchService,private modalService: NgbModal, private datePipe:DatePipe, private router:Router){}

  ngOnInit(): void {
    this.getMatchs();
  }

  // Méthode pour récupérer la liste des matchs
  getMatchs() {
    this.matchService.getListeMatchs().subscribe(
      (matchs) => {
        // Créeer une copie de chaque match avec un objet "objetDateTime" de type Date
        // qui contient l'heure et la date
        const matchsAvecDates = matchs.map(match => ({...match, objetDateTime: new Date
          (`${match.date}T${match.heure}`)}));
        // Trie les matchs en comparant leurs valeur objetDateTime
        matchsAvecDates.sort((a, b) => b.objetDateTime.getTime() - a.objetDateTime.getTime());

        // Je me suis dit qu'on pouvais ce débarasser de la valeur temporaire
        // on peux toujours enlever ça pour la réutiliser si besoin

        // Moyen aussi de remplacer this.matchs par un this.matchsAvecDates si on enlève le undefined
        // histoire de pouvoir utiliser soit la version string soit la version date
        this.matchs = matchsAvecDates.map(match => ({...match, objetDateTime: undefined }));
        this.paginateItems();
      },
      (error) => {
        console.error("Erreur lors de la récupération des joueurs:", error);
      }
    );
  }


  // méthode pour extraire le nom du jour
  extraireNomJour(dateString: string) {
    const date = new Date(dateString);
    return this.datePipe.transform(date, 'EEEE', 'fr');
  }


  // méthode pour extraire l'heure sous format 00H00
  extraireHeureEtMinute(timeString: string): string {
    const [heure, minute] = timeString.split(':');
    const heureFormat = heure + 'H';
    const minuteFormat = minute;
    return `${heureFormat}${minuteFormat}`;
  }

  // méthode pour extraire le jour et le mois
  extraireJourEtMois(dateString: string) {
    const date = new Date(dateString);
    return this.datePipe.transform(date, 'dd MMMM YYYY', 'fr');
  }
  calculerStatutMatch(dateString: string, timeString: string): string {
    const dateMatch = new Date(dateString);
    const [heure, minute] = timeString.split(':');
    dateMatch.setHours(parseInt(heure, 10), parseInt(minute, 10), 0, 0);
    console.log(dateMatch)
    const maintenant = new Date();
    console.log(maintenant)
    const differenceMinutes = Math.round((dateMatch.getTime() - maintenant.getTime()) / (1000 * 60));
    console.log(differenceMinutes)
    if (differenceMinutes > 90) {
      return 'À venir';
    } else if (differenceMinutes <= 0 && differenceMinutes >= -90) {
      return 'En cours';
    } else {
      return 'Terminé';
    }
  }

  //Methodes de PAGINATION
    paginateItems(): void {
      const startIndex = (this.paginationCurrentPage - 1) * this.paginationItemsPerPage;
      const endIndex = startIndex + this.paginationItemsPerPage;
      if (this.matchs) {
        console.log('Start Index:', startIndex);
        console.log('End Index:', endIndex);
        this.paginatedItems = this.matchs.slice(startIndex, endIndex);
      }
    }
    onPageChanged(pageNumber: number): void {
      this.paginationCurrentPage = pageNumber;
      this.paginateItems(); // Rafraîchir les joueurs paginés pour la nouvelle page sélectionnée
    }
  openEditerMatch(match:Match) {
    if (
     this.calculerStatutMatch(match.date,match.heure) == "Terminé"
    ){this.router.navigate(["match-resultat/" , match.id])}
    else {
      const modalRef = this.modalService.open(MatchEditionComponent);
      modalRef.componentInstance.match = match;

      modalRef.result.then((result) => {
        if (result === 'edited') {
          this.getMatchs()
        }
      })
    }
  }
    supprimerMatch(id:number){
      const modalRef = this.modalService.open(SupprimerMatchComponent);
      modalRef.componentInstance.matchId=id;

      modalRef.result.then((result) => {
        if (result === 'deleted') {
          // Actualisez la liste des équipes après
          this.getMatchs();
        }
      });

    }

}
