import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipeVisiteuseComponent } from './equipe-visiteuse.component';

describe('EquipeVisiteuseComponent', () => {
  let component: EquipeVisiteuseComponent;
  let fixture: ComponentFixture<EquipeVisiteuseComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EquipeVisiteuseComponent]
    });
    fixture = TestBed.createComponent(EquipeVisiteuseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
