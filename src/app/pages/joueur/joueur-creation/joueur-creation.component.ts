import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Equipe } from 'src/app/interfaces/equipe';
import { Joueur } from 'src/app/interfaces/joueur';
import { EquipeService } from 'src/app/services/equipe.service';
import { JoueurService } from 'src/app/services/joueur.service';
import { AbstractControl, ValidatorFn } from '@angular/forms';

/**
 * Fonction qui limite l'utilisation des lettres de l'alphabet et de l'espace dans le champs Input
 * @returns 
 */
function noSpecialChars(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const forbidden = /[^a-zA-Z]/.test(control.value);
      return forbidden ? {'noSpecialChars': {value: control.value}} : null;
    };
  }

@Component({
  selector: 'app-joueur-creation',
  templateUrl: './joueur-creation.component.html',
  styleUrls: ['./joueur-creation.component.scss']
})
export class JoueurCreationComponent  {

choixEquipe!: string ;
choixPosition!: string ;
joueur?: Joueur ;
equipeList?: Equipe[];
equipe?: Equipe | null = null;
selectedEquipe!: Equipe ; 
formJoueur: FormGroup = new FormGroup({
    nom: new FormControl(this.joueur, [Validators.required, noSpecialChars()]),
    prenom: new FormControl(this.joueur, [Validators.required, noSpecialChars()]),
    age: new FormControl(this.joueur, 
        [Validators.required, Validators.min(16), Validators.max(50)]),
    nationalite: new FormControl(this.joueur, Validators.required),
    equipe: new FormControl(this.equipe, Validators.required),
    numero: new FormControl(this.joueur, Validators.required),
    position: new FormControl(this.joueur, Validators.required),
    taille: new FormControl(this.joueur,  
        [Validators.required, Validators.min(1.6), Validators.max(2.5)]),
    poids: new FormControl(this.joueur, 
        [Validators.required, Validators.min(60), Validators.max(120)]),
  });


  ngOnInit(): void {  
    this.equipeService.getEquipeList().subscribe(equipeList =>
    this.equipeList = equipeList);
    console.log(this.equipeList)
}

constructor(  
  private joueurService: JoueurService,
  private equipeService: EquipeService,
  private route: ActivatedRoute,
  private http: HttpClient,
  private router: Router
  ){}

/**
 * Vérifie si un champ spécifique du formulaire est invalide et a été touché par l'utilisateur.
 *
 * @param {string} field - Le nom du champ à vérifier.
 * @returns {boolean} Retourne `true` si le champ est invalide et a été touché. Sinon, retourne `false`.
 *
 */
  isFieldValid(field: string) {
    return !this.formJoueur.get(field)?.valid && this.formJoueur.get(field)?.touched;
  }

  creerJoueur() {
    if (this.formJoueur.valid){
        let joueur: Joueur = {} as Joueur;   
        joueur.nom=this.formJoueur.get("nom")!.value as string;
        joueur.prenom=this.formJoueur.get("prenom")!.value as string;
        joueur.nomCourt = joueur.prenom.charAt(0) + '. ' + joueur.nom;
        joueur.nationalite=this.formJoueur.get("nationalite")!.value as string;
        joueur.poids=this.formJoueur.get("poids")!.value as number;
        joueur.taille=this.formJoueur.get("taille")!.value as number;
        joueur.age=this.formJoueur.get("age")!.value as number;
        joueur.position=this.formJoueur.get("position")!.value as string;
        joueur.numero=this.formJoueur.get("numero")!.value as number;
        joueur.equipe=this.formJoueur.get("equipe")!.value as Equipe;
        console.log("recoucou" + joueur) 
        this.joueurService.createJoueur(joueur).subscribe();
        window.alert("Joueur créé avec succès");
        this.router.navigate(['/joueurliste']);
    }
  }


/*---------------- Affichage des Logo des equipes  -----------------*/
getEquipeLogo(selectedEquipe: Equipe): string {

  if (selectedEquipe?.nomLong === "Glasgow Ranger") {
      return "../assets/images/equipeLogo/glasgowRangers.png";
  }
  else if (selectedEquipe?.nomLong == "Celtic Glasgow") {
      return "../assets/images/equipeLogo/celtic.png";
  }
  else if (selectedEquipe?.nomLong == "Heart of Midlothian FC") {
      return "../assets/images/equipeLogo/heart.png";
  }
  else if (selectedEquipe?.nomLong == "Kilmarnock FC") {
      return "../assets/images/equipeLogo/kilmarnock.png";
  }
  else if (selectedEquipe?.nomLong == "St Mirren FC") {
      return "../assets/images/equipeLogo/saintMirren.png";
  }
  else if (selectedEquipe?.nomLong == "Hibernian FC") {
      return "../assets/images/equipeLogo/hibernian.png";
  }
  else if (selectedEquipe?.nomLong == "Dundee FC") {
      return "../assets/images/equipeLogo/fc-dundee-logo.png";
  }
  else if (selectedEquipe?.nomLong == "Motherwell FC") {
      return "../assets/images/equipeLogo/motherwell.png";
  }
  else if (selectedEquipe?.nomLong == "Aberdeen FC") {
      return "../assets/images/equipeLogo/aberdeen.png";
  }
  else if (selectedEquipe?.nomLong == "St Johnstone FC") {
      return "../assets/images/equipeLogo/stJohnstone.png";
  }
  else if (selectedEquipe?.nomLong == "Ross County") {
      return "../assets/images/equipeLogo/rossCounty.png";
  }
  else if (selectedEquipe?.nomLong == "Livingston FC") {
      return "../assets/images/equipeLogo/livingstone.png";
  }
return "../assets/images/equipeLogo/Scottish_Premiership.png";
}


/*---------------- Affichage des drapeaux des nationalités des joueurs -----------------*/
getDrapeau(joueur: Joueur): string {
  if (joueur && joueur.nationalite === "Écossaise") {
      return "../assets/images/drapeaux/ecosse.png";
  }
  else if (joueur && joueur.nationalite === "Française") {
      return "../assets/images/drapeaux/france.png";
  }
  else if (joueur && joueur.nationalite === "Anglaise") {
      return "../assets/images/drapeaux/angleterre.png";
  }
  else if (joueur && joueur.nationalite === "Irlandaise") {
      return "../assets/images/drapeaux/irlande.png";
  }
  else if (joueur && joueur.nationalite === "Italienne") {
      return "../assets/images/drapeaux/italie.png";
  }
  else if (joueur && joueur.nationalite === "Allemande") {
      return "../assets/images/drapeaux/allemagne.png";
  }
  else if (joueur && joueur.nationalite === "Américaine") {
    return "../assets/images/drapeaux/usa.png";
}
  return "../assets/images/drapeaux/olympique.png"; 
}
}