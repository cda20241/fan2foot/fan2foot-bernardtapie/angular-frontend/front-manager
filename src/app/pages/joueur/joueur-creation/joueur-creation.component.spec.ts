import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JoueurCreationComponent } from './joueur-creation.component';

describe('JoueurCreationComponent', () => {
  let component: JoueurCreationComponent;
  let fixture: ComponentFixture<JoueurCreationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [JoueurCreationComponent]
    });
    fixture = TestBed.createComponent(JoueurCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
