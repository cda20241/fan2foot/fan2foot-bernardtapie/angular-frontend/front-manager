import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Joueur } from 'src/app/interfaces/joueur';
import { JoueurService } from 'src/app/services/joueur.service';

@Component({
  selector: 'app-joueur-detail',
  templateUrl: './joueur-detail.component.html',
  styleUrls: ['./joueur-detail.component.scss']
})

export class JoueurDetailComponent implements OnInit{

  joueur!: Joueur;

  constructor(
    private joueurService: JoueurService,
    private route: ActivatedRoute
    ){}
  
    ngOnInit(): void {
      this.joueurService.getJoueur(+this.route.snapshot.paramMap.get(`id`)!)
        .subscribe(joueur => this.joueur=joueur)
    }
    /*************** Fonction qui récupère le drapeau de la nationalité du joueur *************/
    getDrapeau(joueur?: Joueur): string {
      if (joueur?.nationalite === "Écossaise") {
          return "../assets/images/drapeaux/ecosse.png";
      }
      else if (joueur && joueur.nationalite === "Française") {
          return "../assets/images/drapeaux/france.png";
      }
      else if (joueur && joueur.nationalite === "Anglaise") {
          return "../assets/images/drapeaux/angleterre.png";
      }
      else if (joueur && joueur.nationalite === "Irlandaise") {
          return "../assets/images/drapeaux/irlande.png";
      }
      else if (joueur && joueur.nationalite === "Italienne") {
          return "../assets/images/drapeaux/italie.png";
      }
      else if (joueur && joueur.nationalite === "Allemande") {
          return "../assets/images/drapeaux/allemagne.png";
      }
      else if (joueur && joueur.nationalite === "Américaine") {
        return "../assets/images/drapeaux/usa.png";
    }
      return "../assets/images/drapeaux/olympique.png"; 
  }
  
   /*---------------- Affichage d'une photo aléatoire pour le joueur  -----------------*/
  choixPhoto(): string {
    let numPortrait = Math.floor(Math.random() * 14) + 1; 
    return `../assets/images/photoJoueur/portrait${numPortrait}.jpeg`;
}
 /*---------------- Affichage des Logo des equipes  -----------------*/
 getEquipeLogo(joueur : Joueur): string {
  if (joueur.equipe.nomLong === "Glasgow Ranger") {
      return "../assets/images/equipeLogo/glasgowRangers.png";
  }
  else if (joueur.equipe.nomLong == "Celtic Glasgow") {
      return "../assets/images/equipeLogo/celtic.png";
  }
  else if (joueur.equipe.nomLong == "Heart of Midlothian FC") {
      return "../assets/images/equipeLogo/heart.png";
  }
  else if (joueur.equipe.nomLong == "Kilmarnock FC") {
      return "../assets/images/equipeLogo/kilmarnock.png";
  }
  else if (joueur.equipe.nomLong == "St Mirren FC") {
      return "../assets/images/equipeLogo/saintMirren.png";
  }
  else if (joueur.equipe.nomLong == "Hibernian FC") {
      return "../assets/images/equipeLogo/hibernian.png";
  }
  else if (joueur.equipe.nomLong == "Dundee FC") {
      return "../assets/images/equipeLogo/dundee.png";
  }
  else if (joueur.equipe.nomLong == "Motherwell FC") {
      return "../assets/images/equipeLogo/motherwell.png";
  }
  else if (joueur.equipe.nomLong == "Aberdeen FC") {
      return "../assets/images/equipeLogo/aberdeen.png";
  }
  else if (joueur.equipe.nomLong == "St Johnstone FC") {
      return "../assets/images/equipeLogo/stJohnstone.png";
  }
  else if (joueur.equipe.nomLong == "Ross County") {
      return "../assets/images/equipeLogo/rossCounty.png";
  }
  else if (joueur.equipe.nomCourt == "Livingston FC") {
      return "../assets/images/equipeLogo/livingstone.png";
  }
return "../assets/images/equipeLogo/logo-13.png";
}

}
