import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JoueurEditionComponent } from './joueur-edition.component';

describe('JoueurEditionComponent', () => {
  let component: JoueurEditionComponent;
  let fixture: ComponentFixture<JoueurEditionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [JoueurEditionComponent]
    });
    fixture = TestBed.createComponent(JoueurEditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
