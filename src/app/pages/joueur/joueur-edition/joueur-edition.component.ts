import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Equipe } from 'src/app/interfaces/equipe';
import { Joueur } from 'src/app/interfaces/joueur';
import { JoueurService } from 'src/app/services/joueur.service';
import { EquipeService } from 'src/app/services/equipe.service';
import { HttpClient } from '@angular/common/http';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';

/**
 * Fonction qui limite l'utilisation des lettres de l'alphabet et de l'espace dans le champs Input
 * @returns 
 */
function noSpecialChars(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const forbidden = /[^a-zA-Z]/.test(control.value);
      return forbidden ? {'noSpecialChars': {value: control.value}} : null;
    };
  }


@Component({
  selector: 'app-joueur-edition',
  templateUrl: './joueur-edition.component.html',
  styleUrls: ['./joueur-edition.component.scss'],
  
})
export class JoueurEditionComponent {
  joueur!: Joueur;
  joueurs: Joueur[] | null = null;
  equipe?: Equipe | null = null;
  equipeList?: Equipe[];
  choixPosition!: string;
  formJoueur: FormGroup = new FormGroup({
    nom: new FormControl("", [Validators.required, noSpecialChars()]),
    prenom: new FormControl("", [Validators.required, noSpecialChars()]),
    nomCourt: new FormControl("", Validators.required),
    age: new FormControl("", [Validators.required, Validators.min(16), Validators.max(50)]),
    nationalite: new FormControl("", Validators.required),
    equipe: new FormControl("", Validators.required),
    numero: new FormControl("", Validators.required),
    position: new FormControl("", Validators.required),
    taille: new FormControl("", [Validators.required, Validators.min(1.6), Validators.max(2.5)]),
    poids: new FormControl("", [Validators.required, Validators.min(60), Validators.max(120)]),
  });

  constructor(
    private joueurService: JoueurService,
    private equipeService: EquipeService,
    private route: ActivatedRoute,
    private http: HttpClient,
    ){
        this.formJoueur.updateValueAndValidity();
  }
  
    ngOnInit(): void {
      this.joueurService.getJoueur(+this.route.snapshot.paramMap.get(`id`)!)
      .subscribe((joueur) => {this.joueur=joueur; this.initForm()});
        this.equipeService.getEquipeList().subscribe(equipeList =>
          this.equipeList = equipeList);
    }

/**
 * Vérifie si un champ spécifique du formulaire est invalide et a été touché par l'utilisateur.
 *
 * @param {string} field - Le nom du champ à vérifier.
 * @returns {boolean} Retourne `true` si le champ est invalide et a été touché. Sinon, retourne `false`.
 *
 */
isFieldValid(field: string) {
    return !this.formJoueur.get(field)?.valid && this.formJoueur.get(field)?.touched;
  }

/**
 * Fonction du bouton valider qui renvoie les information du joueur mises à jour vers le Service
 */
    valider() {
        if (this.formJoueur.valid){
            let joueur: Joueur = {} as Joueur;   
            joueur.id= this.joueur.id;
            joueur.nom=this.formJoueur.get("nom")!.value as string;
            joueur.prenom=this.formJoueur.get("prenom")!.value as string;
            joueur.nomCourt=this.formJoueur.get("nomCourt")!.value as string;
            joueur.nationalite=this.formJoueur.get("nationalite")!.value as string;
            joueur.poids=this.formJoueur.get("poids")!.value as number;
            joueur.taille=this.formJoueur.get("taille")!.value as number;
            joueur.age=this.formJoueur.get("age")!.value as number;
            joueur.position=this.formJoueur.get("position")!.value as string;
            joueur.numero=this.formJoueur.get("numero")!.value as number;
            joueur.equipe=this.formJoueur.get("equipe")!.value as Equipe;
            console.log(joueur) 
            this.joueurService.updateJoueur(joueur.id, joueur).subscribe();
            window.alert("Joueur Modifié avec succès")
        }
      }
    /*** Initialiser les valeurs du formulaire***/
        initForm(){
            this.formJoueur.patchValue({
                nom: this.joueur.nom,
                prenom: this.joueur.prenom,
                nomCourt: this.joueur.nomCourt,
                age: this.joueur.age,
                nationalite: this.joueur.nationalite,
                equipe: this.joueur.equipe,
                numero: this.joueur.numero,
                position: this.joueur.position,
                taille: this.joueur.taille,
                poids: this.joueur.poids,
            });
        }

      
    /*---------------- Affichage des photos des joueurs en aléatoire -----------------*/
    choixPhoto(): string {
      let numPortrait = Math.floor(Math.random() * 14) + 1; 
      return `../assets/images/photoJoueur/portrait${numPortrait}.jpeg`;
    }

    getAllEquipes() {
    this.equipeService.getEquipeList().subscribe(equipeList =>        
      this.equipeList = equipeList);
    }

    /*---------------- Affichage des Logo des equipes  -----------------*/
    getEquipeLogo(joueur : Joueur): string {
      if (joueur.equipe.nomLong === "Glasgow Ranger") {
          return "../assets/images/equipeLogo/glasgowRangers.png";
      }
      else if (joueur.equipe.nomLong == "Celtic Glasgow") {
          return "../assets/images/equipeLogo/celtic.png";
      }
      else if (joueur.equipe.nomLong == "Heart of Midlothian FC") {
          return "../assets/images/equipeLogo/heart.png";
      }
      else if (joueur.equipe.nomLong == "Kilmarnock FC") {
          return "../assets/images/equipeLogo/kilmarnock.png";
      }
      else if (joueur.equipe.nomLong == "St Mirren FC") {
          return "../assets/images/equipeLogo/saintMirren.png";
      }
      else if (joueur.equipe.nomLong == "Hibernian FC") {
          return "../assets/images/equipeLogo/hibernian.png";
      }
      else if (joueur.equipe.nomLong == "Dundee FC") {
          return "../assets/images/equipeLogo/dundee.png";
      }
      else if (joueur.equipe.nomLong == "Motherwell FC") {
          return "../assets/images/equipeLogo/motherwell.png";
      }
      else if (joueur.equipe.nomLong == "Aberdeen FC") {
          return "../assets/images/equipeLogo/aberdeen.png";
      }
      else if (joueur.equipe.nomLong == "St Johnstone FC") {
          return "../assets/images/equipeLogo/stJohnstone.png";
      }
      else if (joueur.equipe.nomLong == "Ross County") {
          return "../assets/images/equipeLogo/rossCounty.png";
      }
      else if (joueur.equipe.nomCourt == "Livingston FC") {
          return "../assets/images/equipeLogo/livingstone.png";
      }
   return "../assets/images/equipeLogo/logo-13.png";
    }

    /*---------------- Affichage des drapeaux des nationalités des joueurs -----------------*/
    getDrapeau(joueur: Joueur): string {
      if (joueur && joueur.nationalite === "Écossaise") {
          return "../assets/images/drapeaux/ecosse.png";
      }
      else if (joueur && joueur.nationalite === "Française") {
          return "../assets/images/drapeaux/france.png";
      }
      else if (joueur && joueur.nationalite === "Anglaise") {
          return "../assets/images/drapeaux/angleterre.png";
      }
      else if (joueur && joueur.nationalite === "Irlandaise") {
          return "../assets/images/drapeaux/irlande.png";
      }
      else if (joueur && joueur.nationalite === "Italienne") {
          return "../assets/images/drapeaux/italie.png";
      }
      else if (joueur && joueur.nationalite === "Allemande") {
          return "../assets/images/drapeaux/allemagne.png";
      }
      else if (joueur && joueur.nationalite === "Américaine") {
        return "../assets/images/drapeaux/usa.png";
    }
      return "../assets/images/drapeaux/olympique.png"; 
  }
  
  }
  
  