import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RencontreResultatComponent } from './rencontre-resultat.component';

describe('RencontreResultatComponent', () => {
  let component: RencontreResultatComponent;
  let fixture: ComponentFixture<RencontreResultatComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RencontreResultatComponent]
    });
    fixture = TestBed.createComponent(RencontreResultatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
