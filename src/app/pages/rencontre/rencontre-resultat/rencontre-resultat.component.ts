import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {MatchDetails} from "../../../interfaces/matchDetails";
import {ActivatedRoute} from "@angular/router";
import {MatchService} from "../../../services/match.service";

@Component({
  selector: 'app-rencontre-resultat',
  templateUrl: './rencontre-resultat.component.html',
  styleUrls: ['./rencontre-resultat.component.scss']
})
export class RencontreResultatComponent implements OnInit {

  matchId: number | null = null;
  matchDetails$: Observable<MatchDetails> | any;

  constructor(private route: ActivatedRoute, private matchService: MatchService) {}

  ngOnInit(): void {
    this.getMatchIdFromUrl(); // Appel de la methode pour récupéré l'id du match dans l'url
  }

  getMatchIdFromUrl(): void {

    const urlId = this.route.snapshot.paramMap.get('id');
    if (urlId !== null) {
      this.matchId = +urlId;
      this.getMatchDetails(); // Appel de la methode pour récupéré les details du match
    } else {
      console.error("Id du match introuvable dans l'url")
    }
  }


  getMatchDetails(): void {
    if (this.matchId !== null) {
      this.matchDetails$ = this.matchService.getMatchDetails(this.matchId);
    }
  }

}
