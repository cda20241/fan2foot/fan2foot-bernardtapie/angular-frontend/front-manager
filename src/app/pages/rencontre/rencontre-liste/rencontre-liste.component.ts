import { Component } from '@angular/core';
import {MatchCreerComponent} from "../../../pop-ups/match-creer/match-creer.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-rencontre-liste',
  templateUrl: './rencontre-liste.component.html',
  styleUrls: ['./rencontre-liste.component.scss']
})
export class RencontreListeComponent {
  constructor(private modalService: NgbModal){

  }
  openCreerMatch() {
    this.modalService.open(MatchCreerComponent);
  }
}
