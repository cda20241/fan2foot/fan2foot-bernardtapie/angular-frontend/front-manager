import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RencontreListeComponent } from './rencontre-liste.component';

describe('RencontreListeComponent', () => {
  let component: RencontreListeComponent;
  let fixture: ComponentFixture<RencontreListeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RencontreListeComponent]
    });
    fixture = TestBed.createComponent(RencontreListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
