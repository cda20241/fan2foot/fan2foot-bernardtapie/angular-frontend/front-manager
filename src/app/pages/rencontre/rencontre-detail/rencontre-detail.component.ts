import { Component, OnInit } from '@angular/core';
import { MatchService } from 'src/app/services/match.service';
import { ActivatedRoute } from '@angular/router';
import { MatchDetails } from 'src/app/interfaces/matchDetails';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-rencontre-detail',
  templateUrl: './rencontre-detail.component.html',
  styleUrls: ['./rencontre-detail.component.scss']
})

export class RencontreDetailComponent implements OnInit {

  matchId: number | null = null;
  matchDetails$: Observable<MatchDetails> | any;

  constructor(private route: ActivatedRoute, private matchService: MatchService) {}

  ngOnInit(): void {
    this.getMatchIdFromUrl(); // Appel de la methode pour récupéré l'id du match dans l'url
  }

  getMatchIdFromUrl(): void {
    
    const urlId = this.route.snapshot.paramMap.get('id');
    if (urlId !== null) {
      this.matchId = +urlId;
      this.getMatchDetails(); // Appel de la methode pour récupéré les details du match
    } else {
      console.error("Id du match introuvable dans l'url")
    }
  }


  getMatchDetails(): void {
    if (this.matchId !== null) {
      this.matchDetails$ = this.matchService.getMatchDetails(this.matchId);
    }
  }

}
