import { Component } from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {EquipeCreerComponent} from "../../../pop-ups/equipe-creer/equipe-creer.component";

@Component({
  selector: 'app-liste-equipes',
  templateUrl: './liste-equipes.component.html',
  styleUrls: ['./liste-equipes.component.scss']
})
export class ListeEquipesComponent {
  constructor(private modalService: NgbModal){

  }
  openCreerEquipe() {
    this.modalService.open(EquipeCreerComponent);
  }
}
