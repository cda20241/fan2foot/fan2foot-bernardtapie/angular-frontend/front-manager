import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Portemonnaie } from 'src/app/interfaces/portemonnaie';
import { Utilisateur, UtilisateurIdRole } from 'src/app/interfaces/utilisateur';
import { PortemonnaieService } from 'src/app/services/portemonnaie.service';
import { UtilisateurService } from 'src/app/services/utilisateur.service';



@Component({
  selector: 'app-utilisateur-edit',
  templateUrl: './utilisateur-edit.component.html',
  styleUrls: ['./utilisateur-edit.component.scss']
})

export class UtilisateurEditComponent implements OnInit{
  utilisateur!: Utilisateur;
  portemonnaie!: Portemonnaie;
  formUtilisateur: FormGroup = new FormGroup({
    nom: new FormControl("", [Validators.required, noSpecialChars()]),
    prenom: new FormControl("", [Validators.required, noSpecialChars()]),
    email: new FormControl(""),
    date_naissance: new FormControl(""),
  });


  constructor(
    private utilisateurService : UtilisateurService,
    private portemonnaieService : PortemonnaieService,
    private route: ActivatedRoute,
    private router: Router
    ){}
  
    ngOnInit(): void {
      this.utilisateurService.getUtilisateur(+this.route.snapshot.paramMap.get(`id`)!)
        .subscribe((utilisateur) => {this.utilisateur=utilisateur;this.initForm()})
    }


    UtilisateurLogo(utilisateur : Utilisateur): string {
      if (utilisateur.role === "PARIEUR") {
        return "../assets/images/picto/userLogo.png";
      }
      else return "../assets/images/fan2foot_logo.png"
    }

    valider(){
    if (this.formUtilisateur.valid){
      let utilisateur: UtilisateurIdRole = {} as UtilisateurIdRole;   
      utilisateur.id= this.utilisateur.id;
      utilisateur.nom=this.formUtilisateur.get("nom")!.value as string;
      utilisateur.prenom=this.formUtilisateur.get("prenom")!.value as string;
      utilisateur.email=this.formUtilisateur.get("email")!.value as string;
      utilisateur.date_naissance=this.formUtilisateur.get("date_naissance")!.value as Date;
      utilisateur.role=this.utilisateur.role;
      utilisateur.mdp=this.utilisateur.mdp;
      utilisateur.favoris=this.utilisateur.favoris;
      utilisateur.avatar=this.utilisateur.avatar
      console.log(utilisateur) 
      this.utilisateurService.updateUtilisateur(utilisateur.id, utilisateur).subscribe();
      window.alert("Utilisateur Modifié avec succès")
      this.router.navigate(["/utilisateur/liste"])
      
      
  }
  }

     /*** Initialiser les valeurs du formulaire***/
     initForm(){
      this.formUtilisateur.patchValue({
          nom: this.utilisateur.nom,
          prenom: this.utilisateur.prenom,
          email: this.utilisateur.email,
          date_naissance: this.utilisateur.date_naissance,
          role: this.utilisateur.role, 
          favoris: this.utilisateur.favoris,
          mdp: this.utilisateur.mdp,
          avatar: this.utilisateur.avatar
      });

  }

  
}

/**
 * Fonction qui limite l'utilisation des lettres de l'alphabet et de l'espace dans le champs Input
 * @returns 
 */
function noSpecialChars(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const forbidden = /[^a-zA-Z]/.test(control.value);
    return forbidden ? {'noSpecialChars': {value: control.value}} : null;
  };
}