import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Portemonnaie } from 'src/app/interfaces/portemonnaie';
import { Utilisateur } from 'src/app/interfaces/utilisateur';
import { PortemonnaieService } from 'src/app/services/portemonnaie.service';
import { UtilisateurService } from 'src/app/services/utilisateur.service';

@Component({
  selector: 'app-utilisateur-detail',
  templateUrl: './utilisateur-detail.component.html',
  styleUrls: ['./utilisateur-detail.component.scss']
})
export class UtilisateurDetailComponent implements OnInit {
  utilisateur!: Utilisateur;
  portemonnaie!: Portemonnaie;

  constructor(
    private utilisateurService : UtilisateurService,
    private portemonnaieService : PortemonnaieService,
    private route: ActivatedRoute
    ){}
  
    ngOnInit(): void {
      this.utilisateurService.getUtilisateur(+this.route.snapshot.paramMap.get(`id`)!)
        .subscribe(utilisateur => this.utilisateur=utilisateur)
        
      this.portemonnaieService.getPorteMonnaie(+this.route.snapshot.paramMap.get(`id`)!)
        .subscribe(portemonnaie => this.portemonnaie=portemonnaie)
        console.log(this.portemonnaie);
    }


    UtilisateurLogo(utilisateur : Utilisateur): string {
      if (utilisateur.role === "PARIEUR") {
        return "../assets/images/picto/userLogo.png";
      }
      else return "../assets/images/fan2foot_logo.png"
    }
}
