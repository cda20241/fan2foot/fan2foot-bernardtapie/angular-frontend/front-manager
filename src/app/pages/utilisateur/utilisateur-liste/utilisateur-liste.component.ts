import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Utilisateur } from 'src/app/interfaces/utilisateur';
import { UtilisateurService } from 'src/app/services/utilisateur.service';

@Component({
  selector: 'app-utilisateur-liste',
  templateUrl: './utilisateur-liste.component.html',
  styleUrls: ['./utilisateur-liste.component.scss']
})
export class UtilisateurListeComponent implements OnInit{
    
  utilisateur? : Utilisateur | null= null;
  utilisateurListe: Utilisateur[] = []


  ngOnInit(): void {
    this.utilisateurService.getUtilisateursAvecRole().subscribe(
      (utilisateurListe) => {
        this.utilisateurListe = utilisateurListe;
        console.log(this.utilisateurListe)}
    )
  }

  constructor(
    private utilisateurService : UtilisateurService,
    private router: Router,
  ){}





  UtilisateurLogo(utilisateur : Utilisateur): string {
    if (utilisateur.role === "PARIEUR") {
      return "../assets/images/picto/userLogo.png";
    }
    else return "../assets/images/fan2foot_logo.png"
  }


  /****** Fonctions de navigation *******/
  goTo(utilisateur: Utilisateur){
    this.router.navigate(["/utilisateur/", utilisateur.id]);
}

  goToEdit(utilisateur: Utilisateur){
    this.router.navigate(["/utilisateur/edit/", utilisateur.id])
  }

  /******Suppression d'Utilisateur  ********/
  effacerUtilisateur(utilisateur: Utilisateur){
    if(confirm("Voulez-vous supprimer cet utilisateur ?")) {
      this.utilisateurService.deleteUtilisateur(utilisateur.id).subscribe(() => {  
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
          this.router.navigate(['/utilisateur/liste']);
        }); 
      }); 
    }       
  }

}
