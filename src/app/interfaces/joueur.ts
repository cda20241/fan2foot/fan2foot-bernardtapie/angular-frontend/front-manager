import { Equipe } from "./equipe";

export interface Joueur {

  id:number;
  nom: string;
  prenom: string;
  nomCourt: string;
  nationalite: string;
  age: number;
  poids: number;
  taille: number;
  numero: number;
  position : string;
  nbButs: number;
  nbPasseD: number;
  nbMatchs : number;
  equipe: Equipe;
}

export interface JoueurDTO {
  id:number;
  nom: string;
  prenom: string;
  nomCourt: string;
  nationalite: string;
  age: number;
  poids: number;
  taille: number;
  numero: number;
  position : string;
  nbButs: number;
  nbPasseD: number;
  nbMatchs : number;
}

