export interface Rencontre {
  id: number;
  idEqDomicile?: number;
  idEqExterieur?: number;
  date: string;
  heure: string;
}
