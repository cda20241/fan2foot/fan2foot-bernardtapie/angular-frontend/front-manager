export interface MatchDetails {
  id: number;
  date: string;
  heure: string;
  scoreDomicile: number;
  scoreExterieur: number;
  butsDomicile: JoueursDetails[];
  butsExterieur: JoueursDetails[];
  eqDomicile: EquipeDetails;
  eqExterieur: EquipeDetails;
  passesDomicile: JoueursDetails[];
  passesExterieur: JoueursDetails[];
}

export interface EquipeDetails {
  id: number;
  nomLong: string;
  nomCourt: string;
  championnat: string;
  archive: boolean;
  joueurs: JoueursDetails[];
}

  export interface JoueursDetails {
    id: number;
    nomCourt: string;
  }
