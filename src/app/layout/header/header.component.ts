import { AfterViewInit, Component, ElementRef } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements AfterViewInit {
  constructor(private elementRef: ElementRef, public router: Router) {}

  ngAfterViewInit(): void {
    const links = this.elementRef.nativeElement.querySelectorAll('.navbar-link');
    links.forEach((link: HTMLAnchorElement) => {
      link.addEventListener('click', () => {
        links.forEach((l: HTMLAnchorElement) => l.classList.remove('nav-active'));
        link.classList.add('nav-active');
      });
    });
  }
}
